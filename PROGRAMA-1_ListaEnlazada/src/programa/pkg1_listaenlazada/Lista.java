
package programa.pkg1_listaenlazada;

    
    
      public class Lista {
    protected Nodo inicio, fin;  
    public Lista(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio(String elemento){
        inicio = new Nodo(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
  
    
}


